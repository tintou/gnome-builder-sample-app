/* sample-window.c
 *
 * Copyright (C) 2018 Collabora Ltd.
 * Author: Corentin Noël <corentin.noel@collabora.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sample-config.h"
#include "sample-window.h"

struct _SampleWindow
{
  GtkApplicationWindow  parent_instance;

  /* Template widgets */
  GtkHeaderBar        *header_bar;
  GtkLabel            *label;
};

G_DEFINE_TYPE (SampleWindow, sample_window, GTK_TYPE_APPLICATION_WINDOW)

static void
sample_window_class_init (SampleWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Sample/sample-window.ui");
  gtk_widget_class_bind_template_child (widget_class, SampleWindow, header_bar);
  gtk_widget_class_bind_template_child (widget_class, SampleWindow, label);
}

static void
sample_window_init (SampleWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}
