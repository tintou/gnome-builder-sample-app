# Add cross-compilation support to GNOME Builder

When looking at the MinGW plugin, cross-compilation seems to not being part of the current process.

The real issue with the cross-compilation integration in GNOME Builder is that each build system has its own way to handle this. and GNOME Builder has support for many of them (Autotools, CMake, Meson…)

## Cross-compilation with Meson

Cross-compiling softwares using Meson is really simple, and the steps are [well-documented on their website](http://mesonbuild.com/Cross-compilation.html).

We are here using the [test.crossgen](../test.crossgen) file to setup cross-compilation with meson.

The steps are really straightforward:

```sh
meson --prefix=/usr build --cross-file test.crossgen
cd build
ninja
```

the executable is ready to use.

## Issue with IdeDevice

Unfortunately, ide_device_prepare_configuration is only called once at the application startup and isn't called on-demand afterward
So we can't rely on this function to configure the target device.

## IdeBuildSystem

A possible way to implement cross-compilation is to extend the IdeBuildSystem interface to give it native capabilities. It is then up to each IdeBuildSystem-based implementation to do the right thing.

## Using Yocto to test the cross-compilation

I compiled Yocto targetting aarch64 and generated the x86_64 SDK, then everything got installed into `/opt/poky/`, meaning that I was able to use the sysroot plugin giving it `/opt/poky/2.4.2/sysroots/aarch64-poky-linux/` as sysroot path.

In Builder, I use the `Configure Options` field in the Build preferences and add this: `--cross-file /my/path/to/sample/aarch64.crossgen -Dgda=false`, the aarch64.crossgen is bundled with this repository and you might need to change it according to your needs.

That's all, everything should be configured and working by now.

You can try the application with `qemu-aarch64 -L /opt/poky/2.4.2/sysroots/aarch64-poky-linux/ /path/to/compiled/sample`.