# Sysroot build with Flatpak

Currently in use:
 * [org.freedesktop.BaseSdk](https://github.com/flatpak/freedesktop-sdk-base)
    Using a [Yocto](https://www.yoctoproject.org/) base system that contains a few developer tools to build simple applications and glibc.
    Applications can then only require the org.freedesktop.BasePlatform flatpak

 * [org.freedesktop.Sdk](https://github.com/flatpak/freedesktop-sdk-images) (that depends on org.freedesktop.BaseSdk)
    Now a real flatpak that contains the base libraries that are found in many distributions (GLib, Gtk+…).
    Applications can then only require the org.freedesktop.Platform flatpak (that depends on the org.freedesktop.BasePlatform flatpak)

 * [org.gnome.Sdk](https://git.gnome.org/browse/gnome-sdk-images) (that depends on org.freedesktop.Sdk)
    Another real flatpak that contains the basic gnome-specific libraries (GStreamer, GVFS, WebKitGtk+…).
    Applications can then only require the org.gnome.Platform flatpak (that depends on the org.freedesktop.Platform flatpak)

There is an ongoing effort to remove the Yocto dependency and use [BuildStream](https://buildstream.gitlab.io/buildstream/) instead: [freedesktop-sdk](https://gitlab.com/freedesktop-sdk/freedesktop-sdk)

## Some research

 * I tried to create a `org.freedesktop.BaseSdk_armhf` target to understand how the base Sdk was working. My goal here was to create a flatpak (on x86_64) that would run on x86_64 systems but contain all the cross-compilation toolchain to create binaries for the armhf architecture. I still haven't managed to create this base flatpak conaining the additional `gcc` executable.

 * Flatpak dependency declaration is really flexible and allows you to point to a directory and execute some commands so it might be suitable for embedding some sysroots during the build procedure.

## Embedding the sysroot

This repository contains the flatpak file allowing to build within a flatpak environment against the mentioned sysroot.
The solution here remains very hacky because we have to set the `cflags` and `ldflags` by hand for some components, all of this depending on the structure of your sysroot.

To build the flatpak, you first have to configure `com.collabora.builder-sample.json`. Create an archive of you sysroot and change the `path` pointing to it in the `sysroot` module.

Then you only have to execute `flatpak-builder build com.collabora.builder-sample.json`. The build result in in the `build` directory, you can then just copy the executable to your sysroot.