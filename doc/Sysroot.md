# Add sysroot support to GNOME Builder

First let's understand the internal of GNOME Builder, it's a highly extensible application, thereby even core components are plugins.

![What is happening when clicking the "Build" button](Build Request.png)

## Investigating the existing plugins

### Flatpak

The Flatpak plugin does a lot of work there. The interesting part here is that this plugin implements IdeBuildTargetProvier and IdeBuildTarget allowing the user to target the flatpak Runtime that they want.
Of course, creating a Flatpak image involves a lot more work than building so there are many things here to track and download the dependencies, and of course generating the container itself.

### MinGW

> MinGW (Minimalist GNU for Windows) is a free and open source software development environment for creating Microsoft Windows applications.

This plugin might contain useful information as it involves building to a foreign target.
Looking at the code and the result, this plugin doesn't work at all. But it implement some interesting classes: IdeDeviceProvider and IdeDevice.

This means that it is possible to create and select the wanting device target, for instance one might want to build an application for Raspberry Pi (the IdeDevice) by selecting it in the devices list.

We can see that the IdeBuildTarget is a different thing here because one might want, for instance, to build a Flatpak for Raspberry Pi.

## Environment Variables and JHBuild

In order to use the latest GNOME Builder, it is required to setup a JHBuild environment, everything is explained directly in the GNOME Builder website.

The problem is that when running GNOME Builder inside of JHBuild, a new path to the binaries is prepended to the `$PATH` environment variable.

There is a commit removing this variable at startup and only using it when the JHBuild runtime is selected.

## Getting Feedbacks on the implementation

[According to Christian Hergert](https://gitlab.gnome.org/GNOME/gnome-builder/merge_requests/16#note_56350), the GNOME Builder maintainer, We have to subclass the `IdeRuntime` interface in order to create custom sysroots as the Flatpak plugin is doing to use the Flatpak components only.

In order to only deal with the required environment variables, I made the (maybe controversial) IdeHostSubprocessLauncher class. This helped me to actually implement everything and being able to test without being polluted by the JHBuild environment.

## Doing Some Real Sysroot Builds

The Meson build system is completely integrated into traditional environments and uses pkg-config a lot to resolve dependencies and get all the required build flags.

According to [an Autotools article](https://autotools.io/pkgconfig/cross-compiling.html), it is only required to overwrite a few environment variables to have it work properly.

Let's imagine that `$SYSROOT` is the path to your sysroot, here are the variables to set:
```
PKG_CONFIG_DIR=
PKG_CONFIG_LIBDIR=${SYSROOT}/usr/lib/pkgconfig:${SYSROOT}/usr/share/pkgconfig
PKG_CONFIG_SYSROOT_DIR=${SYSROOT}
```

> Note: On Debian-based systems, it is necessary to append the path `${SYSROOT}/usr/lib/x86_64-linux-gnu/pkgconfig` to `PKG_CONFIG_LIBDIR` because of the multilib changes. On Fedora it should probably be `${SYSROOT}/usr/lib64/pkgconfig`. But again this depends on the distribution and the architecture.

Then we need to tell to the compiler to link against the sysroot:
```
CFLAGS=--sysroot=${SYSROOT}
```
