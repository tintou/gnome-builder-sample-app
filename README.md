# Sample GNOME Builder application

Used to validate the sysroot plugin in GNOME Builder

[View the project on Phabricator](https://phabricator.collabora.com/tag/gnome_builder_sysroot/)

## Build this application

This application is using the [meson build system](http://mesonbuild.com).

To simply build this application:

```sh
meson build
cd build
ninja
sudo ninja install
```

This application is used to build using a sysroot.

## The test

To test the sysroot, a simple reference to libgda-5.0 is added. GDA is a GNOME library handling databases.

In order to ensure that everything is working, please ensure that the development headers of libgda-5.0 are not installed in your host system but are available on the target system.

The tests are here done using a chroot environment in Ubuntu Bionic (18.04) in an amd64 system.

- [Research work to add sysroot to GNOME Builder](doc/Sysroot.md)
- [Research work to add cross-compilation to GNOME Builder](doc/Cross-compilation.md)
- [Research work to add sysroot to Flatpak](doc/Flatpak.md)